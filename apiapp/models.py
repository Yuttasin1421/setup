from django.db import models
from django.contrib.auth.models import User

# Create your models here.


class Account(models.Model):
    user_id = models.CharField(max_length=30)
    username = models.CharField(max_length=30)
    password = models.CharField(max_length=30)
    firstname = models.CharField(max_length=50)
    lastname = models.CharField(max_length=50)
    email = models.EmailField(max_length=254)
    dob = models.DateField(null=True)
    image = models.ImageField(null=True)
    Role = [
        ('Ad', 'Admin'),
        ('Cf', 'Chief'),
        ('Of', 'officer')
    ]
    role = models.CharField(max_length=2, choices=Role)

    def __str__(self):
        return "{}-{}".format(self.firstname, self.lastname)
