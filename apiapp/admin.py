from django.contrib import admin
from .models import Account

# Register your models here.


class AccountAdmin(admin.ModelAdmin):
    list_display = [f.name for f in Account._meta.fields]


admin.site.register(Account, AccountAdmin)
